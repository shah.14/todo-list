import React, {useState}from 'react';
import ToDoForm from './ToDoForm';

function TodoList(){
    const [todos, setTodos]=useState([]);
const addToDo= todo=>{
    if(!todo.text ){
        return;
    }

    const newtodos=[todo, ...todos]

    setTodos(newtodos)
    console.log(...todos);

}
    return(
        <div>
            <h1>What's the plan for today?
            </h1>
            <ToDoForm onSubmit={addToDo} />
             
        </div>
    )
}

export default TodoList