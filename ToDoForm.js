import React,{useState} from "react";

function ToDoForm(props){
    const [input, setInput]= useState('')

    const handleChange = e => {
        setInput(e.target.value);
      };
    
    const handleSubmit = e => {
        e.preventDefault();
        props.onSubmit({
          id: Math.floor(Math.random() * 10000),
          text: input
        });
        setInput('');
      };
   
    return(
        <div>
        <form className="todo-form" onSubmit={handleSubmit}>
           
            <input 
            className="todo-input"
            type="text" 
            placeholder="enter your task"
            value={input}
            onChange={handleChange} ></input>
            <button type="submit">+Add</button>
        </form>
        </div>
    )
}

export default ToDoForm